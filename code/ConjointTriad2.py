#https://tHpointer.com/python-how-to-replace-single-or-multiple-characters-in-a-string/
#https://stackoverflow.com/questions/19463556/string-contains-any-character-in-group
import itertools
def proteinCluster(inputString):
    if len(inputString)==0:
        inputString="ERR_LEN"
    else:
        #nggak ada Glx, Asx, Xaa
        inputString=inputString.replace('A', '1')
        inputString=inputString.replace('G', '1')
        inputString=inputString.replace('V', '1')
        inputString=inputString.replace('I', '2')
        inputString=inputString.replace('L', '2')
        inputString=inputString.replace('F', '2')
        inputString=inputString.replace('P', '2')
        inputString=inputString.replace('Y', '3')
        inputString=inputString.replace('M', '3')
        inputString=inputString.replace('T', '3')
        inputString=inputString.replace('S', '3')
        inputString=inputString.replace('H', '4')
        inputString=inputString.replace('N', '4')
        inputString=inputString.replace('Q', '4')
        inputString=inputString.replace('W', '4')
        inputString=inputString.replace('R', '5')
        inputString=inputString.replace('K', '5')
        inputString=inputString.replace('D', '6')
        inputString=inputString.replace('E', '6')
        inputString=inputString.replace('C', '7')
        #sorted("AGVILFPYMTSHNQWRKDEC"+"BJOUXZ")
        notProteinChar = "BJOUXZ"
        if any(character in inputString for character in notProteinChar):
            inputString="ERR_CHAR"
    return inputString
def multireplace(inputString, toBeReplaces, replaceWith):
    for elem in toBeReplaces:
        if elem in inputString:
            inputString=inputString.replace(elem, replaceWith)
    return inputString

#only work with list
def printDuplicate(inputList):
    uniq = seen =  None
    uniq = list()
    seen = set()
    for x in inputList:
        if x in uniq:
            seen.add(x)
        else:
            uniq.append(x)
    return seen

def getCT3(proteinsequence):
    proteinproducts = [p for p in itertools.product([1,2,3,4,5,6,7], repeat=3)]
    len(proteinproducts)
    proteinproducts=sorted(["".join("".join(str(clusternumber) for clusternumber in protein)) for protein in proteinproducts])
    proteinproducts = dict(zip(proteinproducts, [0]*len(proteinproducts)))
    #proteinsequence = "MATQADLMELDMAMEPDRKAAVSHWQQQSYLDSGIHSGATTTAPSLSGKGNPEEEDVDTSQVLYEWEQGFSQSFTQEQVADIDGQYAMTRAQRVRAAMFPETLDEGMQIPSTQFDAAHPTNVQRLAEPSQMLKHAVVNLINYQDDAELATRAIPELTKLLNDEDQVVVNKAAVMVHQLSKKEASRHAIMRSPQMVSAIVRTMQNTNDVETARCTAGTLHNLSHHREGLLAIFKSGGIPALVKMLGSPVDSVLFYAITTLHNLLLHQEGAKMAVRLAGGLQKMVALLNKTNVKFLAITTDCLQILAYGNQESKLIILASGGPQALVNIMRTYTYEKLLWTTSRVLKVLSVCSSNKPAIVEAGGMQALGLHLTDPSQRLVQNCLWTLRNLSDAATKQEGMEGLLGTLVQLLGSDDINVVTCAAGILSNLTCNNYKNKMMVCQVGGIEALVRTVLRAGDREDITEPAICALRHLT"
    proteinsequence = proteinCluster(proteinsequence)
    for x in range(len(proteinsequence)-2):
        #print(proteinsequence[x:x+3])
        proteinproducts[proteinsequence[x:x+3]]+=1
    #norm diluar (hasil jelek jika normalisasi)
    # maxF = max(proteinproducts.values())
    # minF = min(proteinproducts.values())
    # for x in proteinproducts.keys():
    #     proteinproducts[x] = round((proteinproducts[x] - minF) / maxF, 3)
    return proteinproducts

def getCT4(proteinsequence):
    proteinproducts = [p for p in itertools.product([1,2,3,4,5,6,7], repeat=4)]
    len(proteinproducts)
    proteinproducts=sorted(["".join("".join(str(clusternumber) for clusternumber in protein)) for protein in proteinproducts])
    proteinproducts = dict(zip(proteinproducts, [0]*len(proteinproducts)))
    #proteinsequence = "MATQADLMELDMAMEPDRKAAVSHWQQQSYLDSGIHSGATTTAPSLSGKGNPEEEDVDTSQVLYEWEQGFSQSFTQEQVADIDGQYAMTRAQRVRAAMFPETLDEGMQIPSTQFDAAHPTNVQRLAEPSQMLKHAVVNLINYQDDAELATRAIPELTKLLNDEDQVVVNKAAVMVHQLSKKEASRHAIMRSPQMVSAIVRTMQNTNDVETARCTAGTLHNLSHHREGLLAIFKSGGIPALVKMLGSPVDSVLFYAITTLHNLLLHQEGAKMAVRLAGGLQKMVALLNKTNVKFLAITTDCLQILAYGNQESKLIILASGGPQALVNIMRTYTYEKLLWTTSRVLKVLSVCSSNKPAIVEAGGMQALGLHLTDPSQRLVQNCLWTLRNLSDAATKQEGMEGLLGTLVQLLGSDDINVVTCAAGILSNLTCNNYKNKMMVCQVGGIEALVRTVLRAGDREDITEPAICALRHLT"
    proteinsequence = proteinCluster(proteinsequence)
    for x in range(len(proteinsequence)-3):
        #print(proteinsequence[x:x+3])
        proteinproducts[proteinsequence[x:x+4]]+=1
    #norm diluar (hasil jelek jika normalisasi)
    # maxF = max(proteinproducts.values())
    # minF = min(proteinproducts.values())
    # for x in proteinproducts.keys():
    #     proteinproducts[x] = round((proteinproducts[x] - minF) / maxF, 3)
    return proteinproducts
